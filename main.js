const path = require('path');
const { app, BrowserWindow, ipcMain, Menu, Notification, nativeImage } = require('electron');
const Store = require('electron-store');
const store = new Store();

// store.delete("spent")

// Create some data, for the receips
let receips = [];
// Create some data, for the spent
let spent = [];
//Create data to store Window
let win = null;
let receipsWin = null;
let spentWin = null;
let sumSR;

//make the sum of receips and spent
function sumSentReceips() {
    sumSR = 0;
    let sumS = 0;
    let sumR = 0;
    if (store.has('receips')) {
        receips = store.get('receips');
        for (receip of receips) {
            sumR += receip.receipsValue;
        }
        win.webContents.send('sumReceips', sumR);
        console.log(sumR);
    }
    if (store.has('spent')) {
        spent = store.get('spent');
        for (spentX of spent) {
            sumS += spentX.spentValue;
        }
        win.webContents.send('sumSpent', sumS);
        console.log(sumS);
    };
    sumSR = (sumR - sumS).toFixed(2);
    console.log(sumSR);
    win.webContents.send('sumValue', sumSR);
};
// Create the window when the app is ready
app.on('ready', function createWindow() {
    win = new BrowserWindow({ width: 1200, height: 800 });

    win.loadFile(path.join('src', 'index.html'));

    // Listen when the window is finished to load
    win.webContents.on('did-finish-load', function () {

        if (store.has('receips')) {
            receips = store.get('receips');

        }
        if (store.has('spent')) {
            spent = store.get('spent');

        }
        //we made a new sum for receips and spent
        sumSentReceips();
        // Send the cars with the showCars event
        win.webContents.send('showReceips', receips);
        win.webContents.send('showSpent', spent);
    });
});
// Show a new window for add a new car
ipcMain.on('showAddReceipsWindow', function (e, arg) {
    if (receipsWin) {
        return;
    }

    receipsWin = new BrowserWindow({ width: 500, height: 450 });
    receipsWin.loadFile(path.join('src', 'receips.html'));

    // Listen when the window will be closed for destroy it
    receipsWin.on('closed', () => {
        receipsWin = null
    })
});

// Show a new window for add a new car
ipcMain.on('showAddSpentWindow', function (e, arg) {
    if (spentWin) {
        return;
    }

    spentWin = new BrowserWindow({ width: 500, height: 450 });
    spentWin.loadFile(path.join('src', 'spent.html'));

    // Listen when the window will be closed for destroy it
    spentWin.on('closed', () => {
        spentWin = null
    })
});
/****************** */
// Listen for new Spent event
ipcMain.on('newSpent', function (e, newSpent) {
    // Set the id of the spent
    let newId = 1;
    if (spent.length > 0) {
        newId = spent[spent.length - 1].id + 1;
    }
    console.log(newSpent);
    newSpent.id = newId;
    newSpent.spentValue = parseFloat(newSpent.spentValue);
    // Add the new spent to the array
    spent.push(newSpent);

    // Update the file with the new array of  spent
    store.set('spent', spent);
    //we made a new sum for receips and spent
    sumSentReceips();
    // We have to use the main window event, not the event sender that is the new window
    win.webContents.send('showSpent', [newSpent]);

    // Try to close the add spent page
    spentWin.close();
});

// Listen for delete spent event
ipcMain.on('deleteSpent', function (e, spentId) {

    // Delete the spent from the array
    for (let i = 0; i < spent.length; i++) {
        if (spent[i].id === spentId) {
            spent.splice(i, 1);
            break;
        }
    }

    // Update the file with the new array of  spent
    store.set('spent', spent);
    //we made a new sum for receips and spent
    sumSentReceips();
    // Send back a confirmation that the spent is correctly deleted
    e.sender.send('spentDeleted', spentId);

});
/************ */
// Listen for new Receips event
ipcMain.on('newReceips', function (e, newReceips) {
    // Set the id of the receips
    let newId = 1;
    if (receips.length > 0) {
        newId = receips[receips.length - 1].id + 1;
    }
    console.log(newReceips);
    newReceips.id = newId;
    newReceips.receipsValue = parseFloat(newReceips.receipsValue);
    // Add the new receips to the array
    receips.push(newReceips);

    // Update the file with the new array of  receips
    store.set('receips', receips);
    //we made a new sum for receips and spent
    sumSentReceips();
    // We have to use the main window event, not the event sender that is the new window
    win.webContents.send('showReceips', [newReceips]);

    // Try to close the add receips page
    receipsWin.close();
});

// Listen for delete receips event
ipcMain.on('deleteReceips', function (e, receipsId) {

    // Delete the receips from the array
    for (let i = 0; i < receips.length; i++) {
        if (receips[i].id === receipsId) {
            receips.splice(i, 1);
            break;
        }
    }

    // Update the file with the new array of  receips
    store.set('receips', receips);
    //we made a new sum for receips and spent
    sumSentReceips();
    // Send back a confirmation that the receips is correctly deleted
    e.sender.send('receipsDeleted', receipsId);

});