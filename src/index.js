const { ipcRenderer } = require('electron');
const { dialog, Menu, MenuItem} = require('electron').remote;

// Listen for the showreceips event
ipcRenderer.on('showReceips', function (e, receips) {
    // Loop through each car in the array send
    receips.forEach(generateRowTableR);
});
// Listen for the showspent event
ipcRenderer.on('showSpent', function (e, spent) {
    // Loop through each car in the array send
    spent.forEach(generateRowTableS);
});

// Function for generate a row in the table receips
function generateRowTableR(receips) {
    // Retrieve the body of the table
    const tbody = $('#receips');

    // Create the complete row
    const tr = $('<tr id="rowReceips_' + receips.id + '">');
    tr.append('<th scope="row">' + receips.id + '</th>');
    tr.append('<td>' + receips.receipsDate + '</td>');
    tr.append('<td>' + receips.receipsTitle + '</td>');
    tr.append('<td>' + receips.receipsValue + '</td>');
    tr.append('<td><button id="deleteReceips_' + receips.id + '" class="btn-danger"><i class="fa fa-trash"></i></button></td>');

    // Append it to the table
    tbody.append(tr);

    // Create the listener for the delete button
    const deleteBtn = $('#deleteReceips_' + receips.id);
    deleteBtn.on('click', function (e) {
        e.preventDefault();

        // First show a dialog in order to be sure the user really want to delete the car
        dialog.showMessageBox({
            type: 'warning',
            buttons: ['Non', 'Oui'],
            title: 'Confirmation',
            message: 'Êtes-vous sûr de vouloir supprimer cette recette ?'
        }, function (res) {
            if (res === 1) {
                // Send the id of the car for delete it
                ipcRenderer.send('deleteReceips', receips.id);
            }
        });

    })
}
// Function for generate a row in the table spent
function generateRowTableS(spent) {
    // Retrieve the body of the table
    const tbody = $('#spent');

    // Create the complete row
    const tr = $('<tr id="rowSpent_' + spent.id + '">');
    tr.append('<th scope="row">' + spent.id + '</th>');
    tr.append('<td>' + spent.spentDate + '</td>');
    tr.append('<td>' + spent.spentTitle + '</td>');
    tr.append('<td>' + spent.spentValue + '</td>');
    tr.append('<td><button id="deleteSpent_' + spent.id + '" class="btn-danger"><i class="fa fa-trash"></i></button></td>');

    // Append it to the table
    tbody.append(tr);


    // Create the listener for the delete button
    const deleteBtn = $('#deleteSpent_' + spent.id);
    deleteBtn.on('click', function (e) {
        e.preventDefault();
        // First show a dialog in order to be sure the user really want to delete the car
        dialog.showMessageBox({
            type: 'warning',
            buttons: ['Non', 'Oui'],
            title: 'Confirmation',
            message: 'Êtes-vous sûr de vouloir supprimer cette dépense ?'
        }, function (res) {
            if (res === 1) {
                // Send the id of the car for delete it
                ipcRenderer.send('deleteSpent', spent.id);
            }
        });

    })
}
$('#addReceipsWindow').on('click', function (e) {
    e.preventDefault();

    // Send message for open the new window
    ipcRenderer.send('showAddReceipsWindow');
});
$('#addSpentWindow').on('click', function (e) {
    e.preventDefault();

    // Send message for open the new window
    ipcRenderer.send('showAddSpentWindow');
});

// Listen for the deletedSent event
ipcRenderer.on('spentDeleted', function (e, spentId) {
    // Delete the row of the sent from the UI
    $("#rowSpent_" + spentId).remove();
});

// Listen for the deletedReceips event
ipcRenderer.on('receipsDeleted', function (e, receipsId) {
    // Delete the row of the receips from the UI
    $("#rowReceips_" + receipsId).remove();
});
//listen for the new sum of receips less spent
ipcRenderer.on('sumValue', function (e, sumSR) {
    const sum = $("#total tr");
    sum.html('<th scope="row">Total</th><td>' + sumSR + '</td>');
});
//listen for the new sum of receips
ipcRenderer.on('sumReceips',function (e, sumR){
    const sum = $("#totalReceips tr");
    sum.html(' <th colspan="3">Total recettes:</th><td>'+sumR+'</td');
});
//listen for the new sum of spent
ipcRenderer.on('sumSpent',function (e,sumS){
    const sum = $("#totalSpent tr");
    sum.html('<th colspan="3">Total dépenses:</th><td>'+sumS+'</td>');
});

// Function for toggle edition mode
function toggleEditionMode() {
    // Just show or hide the column action
    if(!$("#actionColumnReceips").is(':visible') && !$("[id^=deleteReceips_]").is(':visible')) {
        $("#actionColumnReceips").show();
        $("[id^=deleteReceips_]").show();
    }
    else {
        $("#actionColumnReceips").hide();
        $("[id^=deleteReceips_]").hide();
    }
    if(!$("#actionColumnSpent").is(':visible') && !$("[id^=deleteSpent_]").is(':visible')) {
        $("#actionColumnSpent").show();
        $("[id^=deleteSpent_]").show();
    }
    else {
        $("#actionColumnSpent").hide();
        $("[id^=deleteSpent_]").hide();
    }
}
// Listen for the toggleEditionMode event
ipcRenderer.on('toggleEditionMode', function (e, arg) {
    toggleEditionMode();
});

// Wait for the dome to be ready
$(function () {
    // Create the context menu
    const menu = new Menu();
    menu.append(new MenuItem({
        label: 'Nouvelle recette', click() {
            // Send message for open the new window
            ipcRenderer.send('showAddReceipsWindow');
        }
    }));
    menu.append(new MenuItem({
        label: 'Nouvelle dépense', click() {
            // Send message for open the new window
            ipcRenderer.send('showAddSpentWindow');
        }
    }));
    menu.append(new MenuItem({
        label: 'Activer/Désactiver menu édition', click() {
            toggleEditionMode();
        }
    }));

    // Add event listener for show the new menu when user make a right click
    $(document).on('contextmenu', function (e) {
        e.preventDefault();
        const {remote} = require('electron');
        menu.popup({window: remote.getCurrentWindow()});
    });
});