const { ipcRenderer } = require('electron');
const { dialog } = require('electron').remote;

// Add listener when the form for add new receips is submit
$("#addReceips").on('submit', function (e) {
    e.preventDefault();

    // Serialize the data of the form
    const newReceips = $("#addReceips").serializeArray().reduce(function (obj, item) {
        obj[item.name] = item.value;

        return obj;
    }, {});

    newReceips.receipsValue = parseFloat(newReceips.receipsValue);

    console.log(newReceips);
    // Then send it to the main process for some manipulation
    ipcRenderer.send('newReceips', newReceips);

    this.reset();
});