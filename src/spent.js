const { ipcRenderer } = require('electron');
const { dialog } = require('electron').remote;

// Add listener when the form for add new spent is submit
$("#addSpent").on('submit', function (e) {
    e.preventDefault();

    // Serialize the data of the form
    const newSpent = $("#addSpent").serializeArray().reduce(function (obj, item) {
        obj[item.name] = item.value;

        return obj;
    }, {});

    newSpent.spentValue = parseFloat(newSpent.spentValue);


    console.log(newSpent);
    // Then send it to the main process for some manipulation
    ipcRenderer.send('newSpent', newSpent);

    this.reset();
});